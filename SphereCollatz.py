import sys

def collatz_read (r) :
    """
    read two ints
    r is a reader
    return a list of the two ints, otherwise a list of zeros
    """
    s = r.readline()
    if s == "" :
        return []
    a = s.split()
    return [int(val) for val in a]

memo = [0 for i in range(1000000)]

def collatz_eval (i, j) :
    global memo

    assert i > 0
    assert j > 0

    if j < i :
        curr = i
        i = j
        j = curr
    
    memo[0] = 0
    memo[1] = 1

    max = 0
    count = 0
    for x in range (i, j + 1) :
        count = 0
        if memo[x] != 0 :
            count = memo[x]
        else :  
            y = x;
            while x > 1:
                if x % 2 == 0 :
                    x = x // 2
                    count += 1
                else :
                    x = x + (x >> 1) + 1
                    count += 2

                if x < 1000000 and memo[x] != 0 :
                    count += memo[x]
                    break
            x = y
            memo[x] = count

        if count > max :
            max = count
    
    assert max > 0
    
    return max


def collatz_print (w, i, j, v) :
    w.write(str(i) + " " + str(j) + " " + str(v) + "\n")

def collatz_solve (r, w) :
    while True :
        a = collatz_read(r)
        if not a :
            return
        i, j = a
        val = collatz_eval(i, j)
        collatz_print(w, i, j, val)




collatz_solve(sys.stdin, sys.stdout)
